createNewStore.sah
This file will create a Store with the Name DataDrivenTestStore(today`s date).
Delivery Areas will be created for the new store.
Newly created store will be activated.
It will verify that the strore is created successfully using an alert pop-up.
All the value are saved in 'store.xlsx' excel file.

******************************************************************************

StoreFunctionLibs.sah
Contains all the functions to be executed. Functions written are:
storeCreation - To create new Store
deliveryAreaCreation - To create Delivery Areas 
storeUserCreation - To create Store user
unmapProduct - To unmap Products in Mapped Product tab
mapProductSKUToSore - To map Product to any store

******************************************************************************

VariableLibs.sah
Contains all the variables user in Store tab which are fetched from store.xlsx (excel file)

******************************************************************************

verifyAction.sah
Contains functions to verify the actions performed. Function written are:
verifyStoreCreation - To verify Store creation and Activate store
verifyUserCreation - To verify Store User creation and Activate user

*******************************************************************************

verifyData.sah
Contains function to verify the data to be entered
Contains assertion functions

******************************************************************************

viewProduct.sah
Contains 